FROM python:3.9
EXPOSE 8000
RUN useradd www
WORKDIR /code
RUN python3 -m venv venv
RUN venv/bin/python -m pip install --upgrade pip
COPY ./requirements.txt /code/requirements.txt
RUN venv/bin/pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code/app
USER www
CMD ["venv/bin/uvicorn", "app.main:app", "--host", "0.0.0.0", "--reload", "--reload-dir", "/vault/secrets/", "--reload-include", "dynamic_db_url", "--reload-exclude", "*.py"]
