# Python Vault K8s
**This repository is for demo purposes; therefore MongoDB may use insecure means during setup.**

## Vault Use Cases
- [Vault Agent Injector (Kubernetes)](https://www.vaultproject.io/docs/platform/k8s/injector)
- [Dynamic Credentials (Database secrets engine)](https://www.vaultproject.io/docs/secrets/databases)
- [Encryption as a Service (Transit secrets engine)](https://www.vaultproject.io/docs/secrets/transit)
- [Format-Preserving Encryption (Transform secrets engine)](https://www.vaultproject.io/docs/secrets/transform)

## Pre-Reqs
- External Vault Cluster (with licensing)
- MicroK8s Cluster (with MongoDB)

## MicroK8s Setup
```bash
$ microk8s enable dns rbac ingress hostpath-storage helm3

$ microk8s helm3 repo add hashicorp https://helm.releases.hashicorp.com

$ microk8s helm3 install vault hashicorp/vault --set "global.externalVaultAddr=<EXTERNAL_VAULT_ADDRESS>"

$ microk8s kubectl create secret generic vault-root-ca.crt --from-file vault.pem

$ microk8s kubectl create serviceaccount webapp

$ microk8s kubectl create clusterrolebinding vault-client-auth-delegator \
  --clusterrole=system:auth-delegator \
  --serviceaccount=default:webapp
```

## Vault Setup
```bash
$ vault secrets enable transit

$ vault write -f transit/keys/webapp

$ vault secrets enable transform

$ vault write transform/role/webapp transformations=ccn,ssn

$ vault write transform/transformations/fpe/ccn \
    template="builtin/creditcardnumber" \
    tweak_source=internal \
    allowed_roles=webapp

$ vault write transform/transformations/fpe/ssn \
    template="builtin/socialsecuritynumber" \
    tweak_source=internal \
    allowed_roles=webapp

$ vault secrets enable database

$ vault write database/config/webapp \
    plugin_name=mongodb-database-plugin \
    allowed_roles="webapp" \
    connection_url="mongodb://{{username}}:{{password}}@<K8s_API_SERVER_IP>:<MONGODB_NODE_PORT>/admin" \
    username="root" \
    password="root"

$ vault write -f database/rotate-root/webapp

$ vault write database/roles/webapp \
    db_name=webapp \
    creation_statements='{"db": "admin", "roles": [{"role": "readWrite", "db": "webapp"}] }' \
    default_ttl="10s" \
    max_ttl="60s"

$ vault policy write webapp - <<EOF
path "database/creds/webapp" {
  capabilities = ["read"]
}
path "transit/encrypt/webapp" {
  capabilities = ["update"]
}
path "transit/decrypt/webapp" {
  capabilities = ["update"]
}
path "transform/encode/webapp" {
  capabilities = ["update"]
}
path "transform/decode/webapp" {
  capabilities = ["update"]
}
EOF

$ vault auth enable kubernetes

$ vault write auth/kubernetes/config \
    kubernetes_host="https://<K8s_API_SERVER_IP>:<K8s_API_SERVER_PORT>" \
    kubernetes_ca_cert=@<PATH_TO_K8s_CA_CERT>

$ vault write auth/kubernetes/role/webapp \
    bound_service_account_names=webapp \
    bound_service_account_namespaces=default \
    policies=webapp \
    ttl=24h

$ exit
```
