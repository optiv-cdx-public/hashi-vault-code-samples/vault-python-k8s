from app.config import settings
from app.routes import sys_internal, users

from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from motor.motor_asyncio import AsyncIOMotorClient


app = FastAPI()
app.include_router(users.router)
app.include_router(sys_internal.router)


@app.on_event("startup")
async def startup():
    app.db_client = AsyncIOMotorClient(settings.db.dynamic_db_url)
    app.db = app.db_client["webapp"]


@app.on_event("shutdown")
async def shutdown():
    app.db_client.close()


@app.get("/", include_in_schema=False)
async def root():
    return RedirectResponse("/docs")
