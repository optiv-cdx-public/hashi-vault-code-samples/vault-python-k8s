from base64 import b64decode, b64encode
import hvac

TRANSIT_KEY = "webapp"
TRANSFORM_ROLE = "webapp"

vault = hvac.Client(url="http://localhost:8200")


def encrypt(plaintext):
    b64_plaintext = b64encode(plaintext.encode()).decode()
    response = vault.secrets.transit.encrypt_data(
        name=TRANSIT_KEY, plaintext=b64_plaintext)
    return response["data"]["ciphertext"]


def decrypt(ciphertext):
    response = vault.secrets.transit.decrypt_data(
        name=TRANSIT_KEY, ciphertext=ciphertext)
    return b64decode(response["data"]["plaintext"]).decode()


def encode(value, transformation):
    response = vault.secrets.transform.encode(
        role_name=TRANSFORM_ROLE, value=value, transformation=transformation)
    return response['data']['encoded_value']


def decode(value, transformation):
    response = vault.secrets.transform.decode(
        role_name=TRANSFORM_ROLE, value=value, transformation=transformation)
    return response['data']['decoded_value']
