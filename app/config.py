from pydantic import BaseModel, BaseSettings


class AppSettings(BaseSettings):
    name: str = "webapp"
    version: str = "v0.1.0"
    environment: str = "dev"

    class Config:
        env_prefix = "app_"


class DatabaseSettings(BaseSettings):
    dynamic_db_url: str

    class Config:
        secrets_dir = "/vault/secrets"


class Settings(BaseModel):
    app: AppSettings = AppSettings()
    db: DatabaseSettings = DatabaseSettings()


settings = Settings()
