from app.vault import encrypt, encode
from fastapi import APIRouter, Request
from pydantic import BaseModel, Field
from fastapi.encoders import jsonable_encoder
from bson import ObjectId
from typing import List


router = APIRouter(tags=["users"])


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")


class User(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    name: str
    pseudonym: str
    ccn: str
    ssn: str

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "name": "Jane Doe",
                "pseudonym": "Moonlight",
                "ccn": "5105105105105100",
                "ssn": "000-12-3456",
            }
        }


@router.get("/users/", response_description="List all users", response_model=List[User])
async def list_users(request: Request):
    return await request.app.db.users.find().to_list(100)


@router.post("/users/", response_description="Add new user", response_model=User)
async def create_user(user: User, request: Request):
    user.pseudonym = encrypt(user.pseudonym)
    user.ccn = encode(user.ccn, transformation="ccn")
    user.ssn = encode(user.ssn, transformation="ssn")
    user = jsonable_encoder(user)
    new_user = await request.app.db.users.insert_one(user)
    return await request.app.db.users.find_one({"_id": new_user.inserted_id})
