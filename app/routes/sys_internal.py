from app.config import settings
from app.routes.users import User
from app.vault import decrypt, decode
from fastapi import APIRouter, Request, HTTPException


router = APIRouter(tags=["internal"])


@router.get("/sys/internal/settings", response_description="View Internal Settings")
async def sys_internal_settings():
    return settings


@router.get("/sys/internal/expose/{id}", response_description="Expose a User", response_model=User)
async def sys_internal_expose(id: str, request: Request):
    if (user := await request.app.db.users.find_one({"_id": id})) is not None:
        user["pseudonym"] = decrypt(user["pseudonym"])
        user["ccn"] = decode(user["ccn"], transformation="ccn")
        user["ssn"] = decode(user["ssn"], transformation="ssn")
        return user

    raise HTTPException(status_code=404, detail=f"User {id} not found")
